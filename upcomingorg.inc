<?php


function upcomingorg_save_event($event) {
  global $user;

  if ($event['id']) {
    $query = 'SELECT nid FROM {upcomingorg_event} WHERE event_id='.intval($event['id']);
    $nid = db_result(db_query($query));
  }

  if ($nid) {
    $node = node_load($nid);
    $op = 'update'; // FIXME: for debug output, remove later
  }
  else {
    $node = new StdClass();
    $node->uid = $user->uid;
    $node->type = variable_get('upcomingorg_event_nodetype', '');
    $node->created = strtotime($event['date_posted']);
    // The following is copied from node_validate()
    $node_options = variable_get('node_options_'. $node->type, array('status', 'promote'));
    $node->status = in_array('status', $node_options);
    $node->moderate = in_array('moderate', $node_options);
    $node->promote = in_array('promote', $node_options);
    $node->sticky = in_array('sticky', $node_options);
    $node->revision = in_array('revision', $node_options);
    $op = 'add'; // FIXME: for debug output, remove later
  }

  $node->title = $event['name'];
  $node->changed = time();
  $node->event_id = $event['id'];
  $fields = array('metro_id', 'venue_nid', 'user_id', 'category_id', 'description', 'tags', 'personal', 'selfpromotion', 'event_start', 'event_end', 'is_imported');
  foreach ($fields as $field) {
    $node->$field = $event[$field];
  }

  // Should we call node_validate()? If so: $node->created has to be set again after validating,
  // since node_validate() overwrites it for users without node administration privileges
  node_save($node);
  #echo "<!-- Saved node {$node->nid} for event $event[id] ($op) -->\n";
  return $node->nid;
} // function location_add_event


function upcomingorg_save_venue($venue) {
  global $user;

  if ($venue['id']) {
    $query = 'SELECT nid FROM {upcomingorg_venue} WHERE venue_id='.intval($venue['id']);
    $nid = db_result(db_query($query));
  }

  if ($nid) {
    // update the already-imported venue
    $node = node_load($nid);
  }
  else {
    $node = new StdClass();
    $node->type = variable_get('upcomingorg_venue_nodetype', '');
    $node->uid = $user->uid;
    $node->title = $venue['name'];
    $node->location = array();
    // The following is copied from node_validate()
    $node_options = variable_get('node_options_'. $node->type, array('status', 'promote'));
    $node->status = in_array('status', $node_options);
    $node->moderate = in_array('moderate', $node_options);
    $node->promote = in_array('promote', $node_options);
    $node->sticky = in_array('sticky', $node_options);
    $node->revision = in_array('revision', $node_options);
  }

  $node->location['name'] = $venue['name'];
  $node->location['street'] = $venue['address'];
  $node->location['city'] = $venue['city'];
  $node->location['postal_code'] = $venue['zip'];

  $node->changed = time();
  $node->venue_id = $venue['id'];
  $fields = array('user_id', 'description', 'url', 'phone', 'private', 'is_imported');
  foreach ($fields as $field) {
    $node->$field = $venue[$field];
  }

  node_save($node);
  echo "<!-- Saved node {$node->nid} for venue $venue[id] -->\n";
  return $node->nid;
}


function upcomingorg_save_category($category)
{
  db_query("INSERT INTO {upcomingorg_category} (category_id, name, description) VALUES (%d, '%s', '%s')", $category['id'], $category['name'], $category['description']);
}
