

Usage
=====

To import events from upcoming.org, visit the page upcomingorg/settings and
configure accordingly (you will need an upcoming.org account for this). This
configuration is on a per-user basis. After that,
you can start the import process by visiting the page upcomingorg (e.g.
www.example.com/upcomingorg). Right now a completely blank page is
displayed, but this will be fixed shortly. You should have shiny new events and
venues in Drupal now. Please note that right now these nodes are not promoted to
front page, but you can find the events in the calendar (www.example.com/event).

To synchronize your imported events and venues (update them with the possibly
changed data from upcoming.org), just visit www.example.com/upcomingorg again.


Help advancing the module
=========================

There are three main things you can do to help this module: find bug, report
feature wishes and review the code.

 1) Finding bugs
    Just test every possible option and usage scenario. Create or join loads of
    events on upcoming.org and import them. Change them locally, change them on
    upcoming.org. Synchronize and check that the event in Drupal is again
    identical to the one on upcoming.org. If you have the time and resources,
    setup a second Drupal site and configure it to import events from your
    first site. You can enter bugs for this module at
    http://drupal.org/node/add/project_issue/upcomingorg/bug

 2) Feature requests
    Right now I am developing according to my project application for Google's
    Summer of Code Program. But maybe you have additional functionality in mind,
    or some things don't work the way you think they should. Please share your
    ideas at http://drupal.org/node/add/project_issue/upcomingorg/feature

 3) Code review
    You are a Drupal coder. You've written one or more modules of your own, you
    hang out on #drupal and you generally know your way around the Drupal
    source. Please share your knowledge with me. :-) Take a look at the module
    source. If you see things which should be done differently, write me! Is
    this the correct way to save or update nodes? Should user authentication be
    done this way? Upcoming.org users have their own IDs, which user ID should
    be reported if Drupal acts as master repository, the upcoming.org one or
    the Drupal one?


Contact
=======

Still have problems? Drop me an email at sj@sjaensch.org, I'll do my best to
help you out.
